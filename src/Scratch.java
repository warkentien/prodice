import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Created by sethf_000 on 10/28/2015.
 */
public class Scratch {
    public static void main(String[] args) {
        JFrame frame = new JFrame();

        final int FRAME_WIDTH = 300;
        final int FRAME_HEIGHT = 400;
        frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        frame.setTitle("Mah New Frame");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

        JButton button = new JButton("A Great Button!");
        JLabel label = new JLabel("Hey fellas");
        JPanel panel = new JPanel();
        panel.add(button);
        panel.add(label);
        frame.add(panel);

        ActionListener listener = new ClickListener();
        button.addActionListener(listener);
    }


}
