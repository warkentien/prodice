package P10_2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by sethf_000 on 10/29/2015.
 *
 * This class handles the action when the button is clicked.
 */
public class ClickListener implements ActionListener {

    //n counts how many times the button was clicked and displays the value in println
    private int n = 0;

    //method called when button is clicked
    public void actionPerformed(ActionEvent event) {
        n++;

        if (n == 1){
            System.out.println("I was clicked " + n + " time!");
        }

        else {
            System.out.println("I was clicked " + n + " times!");
        }
    }




}
