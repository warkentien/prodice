package P10_2;

import javax.swing.*;

/**
 * Created by sethf_000 on 10/29/2015.
 *
 * This class enhances the ButtonFrame1 program in the book to add counter to the number of
 * clicks and display in the console how many times the button has been clicked.
 */
public class Driver {
    public static void main(String[] args) {

        //create the frame object, set close functionality and set visibility
        JFrame frame = new ButtonFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }
}


//Enhance the ButtonViewer1 program in Section 10.2.1 so that it prints a message
//        �I was clicked n times!� whenever the button is clicked. The value n should be incremented
//        with each click.