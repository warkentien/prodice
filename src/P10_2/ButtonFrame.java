package P10_2;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Created by sethf_000 on 10/29/2015.
 *
 * This class creates our button frame to display the button
 */
public class ButtonFrame extends JFrame{

    //set frame heigh and width
    private static final int WIDTH = 300;
    private static final int HEIGHT = 300;

    //constructor
    public ButtonFrame()
    {
        createComponents();
        setSize(WIDTH, HEIGHT);
    }

    //create button and panel
    private void createComponents()
    {
        JButton button = new JButton("Click here!");
        JPanel panel = new JPanel();
        panel.add(button);
        add(panel);

        //set ActionListener on the button
        ActionListener listener = new ClickListener();
        button.addActionListener(listener);
    }

}
