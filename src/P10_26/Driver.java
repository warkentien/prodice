package P10_26;

import javax.swing.*;

/**
 * Created by sethf_000 on 10/29/2015.
 *
 * This class creates a frame and sets the dimensions. It then creates an OlympicRingsComponent
 * object to draw the Olympic rings.
 */
public class Driver {
    public static void main(String[] args) {

        JFrame frame = new JFrame();

        frame.setSize(400, 400);
        frame.setTitle("Olympic Rings");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JComponent component = new OlympicRingsComponent();
        frame.add(component);

        frame.setVisible(true);
    }
}


//Write a program that displays the Olympic rings. Color the rings in the Olympic
//        colors. Provide a method drawRing that draws a ring of a given position and color.