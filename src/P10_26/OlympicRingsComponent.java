package P10_26;

import javax.swing.*;
import java.awt.*;

/**
 * Created by sethf_000 on 10/29/2015.
 *
 * This class draws the olympic rings by calling the drawRing method and passing
 * the position and color information in RGB values
 */
public class OlympicRingsComponent extends JComponent {

    public void paintComponent(Graphics g)
    {
        //cast to 2D object to widen the line we are drawing
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(5));

        //blue ring
        drawRing(g, 100, 100, 50, 50, 0, 122, 201);
        //black ring
        drawRing(g, 150, 100, 50, 50, 0, 0, 0);
        //red ring
        drawRing(g, 200, 100, 50, 50, 225,14,73);
        //yellow ring
        drawRing(g, 125, 125, 50, 50, 255,161,0);
        //green ring
        drawRing(g, 175, 125, 50, 50, 0,155,58);

    }

    //draws a ring after receiving g object, x position, y position, width, height, and rgb values for the colors
    public void drawRing(Graphics g, int x, int y, int width, int height, int red, int green, int blue){

        g.setColor(new Color(red, green, blue));
        g.drawArc(x, y, width, height, 360, 360);
    }

}
