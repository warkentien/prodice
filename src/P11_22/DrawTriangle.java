package P11_22;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by sethf_000 on 10/30/2015.
 *
 * This class creates a graphical application that allows a user to create a triangle by clicking three
 * times. The fourth click both erases the previous triangle and starts a new triangle.
 */
public class DrawTriangle {

    //create panel, point locations and click counter
    private JPanel mPanel = new JPanel();
    private Point mOne, mTwo, mThree;
    private int count;

    //set up frame
    public static void main(String[] args) {
        JFrame frame = new JFrame("DrawTriangle");
        frame.setContentPane(new DrawTriangle().mPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(600, 400);
        frame.setVisible(true);
    }

    //inner class that includes mouse actions and graphics drawing
    public DrawTriangle() {

        mPanel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

                //first/fourth click
                if(count == 0){
                    count = 1;
                    mPanel.getGraphics().clearRect(0, 0, 600, 400);
                    mOne = e.getPoint();
                    mPanel.getGraphics().fillOval(mOne.x, mOne.y, 4, 4);

                }
                //second click
                else if(count == 1){
                    count = 2;
                    mTwo = e.getPoint();
                    mPanel.getGraphics().drawLine(mOne.x, mOne.y, mTwo.x, mTwo.y);
                }
                //third click
                else if(count == 2){
                    count = 0;
                    mThree = e.getPoint();
                    mPanel.getGraphics().drawLine(mTwo.x, mTwo.y, mThree.x, mThree.y);
                    mPanel.getGraphics().drawLine(mThree.x, mThree.y, mOne.x, mOne.y);
                }

            }

            //unneeded mouse methods
            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }
}


//Write a program that allows the user to specify a triangle with three mouse presses.
//        After the first mouse press, draw a small dot. After the second mouse press, draw a
//        line joining the first two points. After the third mouse press, draw the entire triangle.
//        The fourth mouse press erases the old triangle and starts a new one.