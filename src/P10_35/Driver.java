package P10_35;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

/**
 * Created by sethf_000 on 10/29/2015.
 *
 * This class creates a graphical application that allows the user to select one of 10
 * food items. The program keeps track of the total cost and shows a suggested tip and
 * the tax. The user can also add a different item and it's cost and this will be added
 * to the total.
 */
public class Driver {
    //set the frame
    private static final int FRAME_WIDTH = 600;
    private static final int FRAME_HEIGHT = 600;

    //set the text fields and area
    private static final JTextArea resultArea = new JTextArea("Total: $", 8, 20);
    private static final JTextField textField1 = new JTextField(10);
    private static final JTextField textField2 = new JTextField(10);

    public static void main(String[] args) {

        JFrame frame = new JFrame();

        //create and add a top panel and lower panel
        JPanel panel = new JPanel();
        frame.add(panel);
        JPanel lowerPanel = new JPanel();
        frame.getContentPane().add(lowerPanel, "South");

        //add text fields to lower panel
        JLabel textField1Label = new JLabel("Enter an different item: ");
        JLabel textField2Label = new JLabel("Enter the price: ");
        textField1.setEditable(true);
        textField2.setEditable(true);
        resultArea.setEditable(false);
        lowerPanel.add(textField1Label);
        lowerPanel.add(textField1);
        lowerPanel.add(textField2Label);
        lowerPanel.add(textField2);

        //button to submit added item price
        final JButton button12 = new JButton("Add Item");
        button12.setActionCommand("12");
        lowerPanel.add(button12);
        lowerPanel.add(resultArea);

        //create 10 food item buttons
        final JButton button1 = new JButton("Chicken Soup $2.00");
        final JButton button2 = new JButton("Duck Soup $3.50");
        final JButton button3 = new JButton("Garden Salad: $4.00");
        final JButton button4 = new JButton("Chef's Salad: $4.00");
        final JButton button5 = new JButton("Steak: $10.00");
        final JButton button9 = new JButton("Roast Beef: $8.00");
        final JButton button6 = new JButton("Fried Chicken: $8.00");
        final JButton button7 = new JButton("Baked Potato: $3.00");
        final JButton button8 = new JButton("Green Beans:$3.00");
        final JButton button10 = new JButton("Ice Cream: $2.00");

        //add action command to each button so we can differentiate them in the ActionListener
        button1.setActionCommand("1");
        button2.setActionCommand("2");
        button3.setActionCommand("3");
        button4.setActionCommand("4");
        button5.setActionCommand("5");
        button6.setActionCommand("6");
        button7.setActionCommand("7");
        button8.setActionCommand("8");
        button9.setActionCommand("9");
        button10.setActionCommand("10");

        //add the buttons to top panel
        panel.add(button1);
        panel.add(button2);
        panel.add(button3);
        panel.add(button4);
        panel.add(button5);
        panel.add(button6);
        panel.add(button7);
        panel.add(button8);
        panel.add(button9);
        panel.add(button10);

        //create reset total button and add to lower panel
        final JButton button11 = new JButton("Reset Total");
        button11.setActionCommand("11");
        lowerPanel.add(button11);

        frame.add(panel);


        /**
         This is called when any of the buttons are clicked. It uses a switch
         to differentiate the buttons based on their action commands.
         */
        class AddPriceListener implements ActionListener
        {
            private double total;
            public void actionPerformed(ActionEvent event)
            {
                int action = Integer.parseInt(event.getActionCommand());
                switch (action) {
                    case 1:
                        total += 2;
                        break;
                    case 2:
                        total += 3.5;
                        break;
                    case 3:
                        total += 4;
                        break;
                    case 4:
                        total += 4;
                        break;
                    case 5:
                        total += 10;
                        break;
                    case 6:
                        total += 8;
                        break;
                    case 7:
                        total += 3;
                        break;
                    case 8:
                        total += 3;
                        break;
                    case 9:
                        total += 3;
                        break;
                    case 10:
                        total += 2;
                        break;
                    case 11:
                        total = 0;
                        break;
                    case 12:
                        if(textField2.getText().isEmpty()){
                            break;
                        }
                        String string = textField2.getText();

                        try {
                            int price = Integer.parseInt(string);
                            total += price;
                        }
                        catch (NumberFormatException e){
                            break;
                        }

                        break;
                    default:
                        break;
                }

                resultArea.setText("");

                //do some calculations to get tip, tax and total
                double tip = total * .18;
                double tax = total * .08;

                total = total + tax;
                DecimalFormat f = new DecimalFormat("##.00");

                resultArea.append("Suggested 18% tip: $" + f.format(tip));
                resultArea.append("\n8% sales tax: $" + f.format(tax));
                resultArea.append("\n");
                resultArea.append("\nTotal: $" + f.format(total));

            }


        }

        //add our action listener to all buttons
        ActionListener listener = new AddPriceListener();
        button1.addActionListener(listener);
        button2.addActionListener(listener);
        button3.addActionListener(listener);
        button4.addActionListener(listener);
        button5.addActionListener(listener);
        button6.addActionListener(listener);
        button7.addActionListener(listener);
        button8.addActionListener(listener);
        button9.addActionListener(listener);
        button10.addActionListener(listener);
        button11.addActionListener(listener);
        button12.addActionListener(listener);

        //set frame size and make sure program exits on close
        frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }
}


//        Write a graphical application that produces a restaurant bill.
//        Provide buttons for ten popular dishes or drink items. (You
//        decide on the items and their prices.) Provide text fields for
//        entering less popular items and prices. In a text area, show the
//        bill, including tax and a suggested tip.