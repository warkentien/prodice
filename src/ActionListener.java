import java.awt.event.ActionEvent;

/**
 * Created by sethf_000 on 10/28/2015.
 */
public interface ActionListener extends java.awt.event.ActionListener {

    void actionPerformed(ActionEvent event);

}
