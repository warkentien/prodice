package pig;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.Random;

/**
 * Created by sethf_000 on 10/30/2015.
 *
 * This class lets a user play the pig dice game against a computer. Your turn value accumulates
 * and if you select hold, your turn value gets added to your overall value. If either player rolls
 * a one, they are awarded no points that turn and the other player takes over. The first player to
 * reach 100 wins.
 * The human player has to press the roll button for the computer to simulate the computer's roll.
 */
public class Driver {
    //set the frame
    private static final int FRAME_WIDTH = 600;
    private static final int FRAME_HEIGHT = 600;

    //set the text area
    private static final JTextArea resultArea = new JTextArea("Total: ", 8, 20);


    public static void main(String[] args) {

        JFrame frame = new JFrame();

        //create and add a top panel and lower panel
        JPanel panel = new JPanel();
        frame.add(panel);
        JPanel lowerPanel = new JPanel();
        frame.getContentPane().add(lowerPanel, "South");

        //disallow editing of text area and add it to lower panel
        resultArea.setEditable(false);
        lowerPanel.add(resultArea);

        //create roll and hold buttons
        final JButton button1 = new JButton("Roll");
        final JButton button2 = new JButton("Hold");

        //add action command to each button so we can differentiate them in the ActionListener
        button1.setActionCommand("1");
        button2.setActionCommand("2");

        //add the buttons to top panel
        panel.add(button1);
        panel.add(button2);

        frame.add(panel);


        /**
         This is called when any of the buttons are clicked. It uses a switch
         to differentiate the buttons based on their action commands.
         */
        class AddPriceListener implements ActionListener
        {
            //set up our counter variables
            private int die_value;
            private int computerHold;
            private int total;
            private int computerTotal;
            private int turnTotal;
            private boolean player;
            public void actionPerformed(ActionEvent event)
            {
                int action = Integer.parseInt(event.getActionCommand());
                //when the roll button is clicked
                switch (action) {
                    case 1:
                        Random rand = new Random();
                        //get die value
                        die_value = rand.nextInt(6) +1;

                        if (!player){
                            computerHold = 2;
                        }
                        //if it's computer's turn, randomize it's decision to hold or not
                        else if (player){
                            computerHold = rand.nextInt(4) +1;;
                        }
                        //calculate turntotal for either player
                        turnTotal += die_value;
                        //if die is 1, return no points and end turn
                        if(die_value == 1){
                            resultArea.setText("");
                            resultArea.append("Die Value: " + die_value);
                            resultArea.append("\n**NO POINTS AWARDED!**");
                            turnTotal = 0;
                            if (!player){
                                resultArea.append("\nHuman's Current Total: " + total);
                            }
                            else if (player){
                                resultArea.append("\nComputer's Current Total: " + computerTotal);
                            }

                            resultArea.append("\n\nNext Player's Turn");
                            player = !player;
                        }
                        //if computer was randomized to choose hold
                        else if(computerHold < 2){
                            resultArea.setText("");
                            resultArea.append("** COMPUTER HOLDS **");
                            computerTotal += turnTotal;
                            resultArea.append("\nDie Value: " + die_value);
                            resultArea.append("\nTurn Total: " + turnTotal);
                            resultArea.append("\nComputer's Current Total: " + computerTotal);
                            turnTotal = 0;
                            resultArea.append("\n\nNext Player's Turn");
                            player = !player;
                        }
                        //else, add the current die total to turn total
                        else{
                            resultArea.setText("");
                            if (!player){
                                resultArea.append("Human's Turn:");
                                resultArea.append("\nDie Value: " + die_value);
                                resultArea.append("\nTurn Total: " + turnTotal);
                                resultArea.append("\nOverall Total: " + total);
                            }
                            else if (player){
                                resultArea.append("Computer's Turn:");
                                resultArea.append("\nDie Value: " + die_value);
                                resultArea.append("\nTurn Total: " + turnTotal);
                                resultArea.append("\nOverall Total: " + computerTotal);
                            }

                        }

                        break;

                    //if the hold button was clicked
                    case 2:
                        resultArea.setText("");
                        resultArea.append("**HOLD**");

                        //add current turn total to overall total and end turn
                        if (!player){
                            total += turnTotal;
                            resultArea.append("\nHuman's Current Total: " + total);
                        }
                        else if (player){
                            computerTotal += turnTotal;
                            resultArea.append("\nComputer's Current Total: " + computerTotal);
                        }
                        turnTotal = 0;
                        resultArea.append("\n\nNext Player's Turn");
                        player = !player;
                        break;

                    default:
                        break;
                }

                //if either player's total hits 100, game over
                if (total >= 100){
                    resultArea.setText("");
                    resultArea.append("*****HUMAN WINS!****");
                    resultArea.append("\nHuman's Total: " + total);
                    total = 0;
                    computerTotal = 0;
                }
                else if (computerTotal >= 100){
                    resultArea.setText("");
                    resultArea.append("*****COMPUTER WINS!****");
                    resultArea.append("\nComputer's Total: " + computerTotal);
                    total = 0;
                    computerTotal = 0;
                }



            }


        }

        //add our action listener to all buttons
        ActionListener listener = new AddPriceListener();
        button1.addActionListener(listener);
        button2.addActionListener(listener);

        //set frame size and make sure program exits on close
        frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }
}
